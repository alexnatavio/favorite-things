Welcome to the first Git exercise!
Completing these prompts will help you get more comfortable with
Git.

1. What is your favorite color?
Garnet

2. What is your favorite food?
Pho

3. Who is your favorite fictional character?
Jay Gatsby

4. What is your favorite animal?
Raccoon

5. What is your favorite programming language? (Hint: You can always say Python!!)
Python
